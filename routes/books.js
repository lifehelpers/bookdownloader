var express = require('express');
var router = express.Router();
var libgen = require('libgen');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.get('/search/:isbn', function(req, res) {

    // Get /musician/Matt
    //console.log(req.params.name)
    // => Matt
    options.query = req.params.isbn;
    libgen.search(options, (err, data) => {
        if (err)
            return console.error(err);
        let n = data.length;
        console.log(n + ' most recently published "' +
            options.query + '" books');
        while (n--){
            console.log('***********');
            console.log('Title: ' + data[n].title);
            console.log('Author: ' + data[n].author);
            console.log('{"Download: ' + '"http://gen.lib.rus.ec/book/index.php?md5=' + data[n].md5.toLowerCase() + '"}');
            res.send('{"Download": ' + '"http://gen.lib.rus.ec/book/index.php?md5=' + data[n].md5.toLowerCase() + '"}');
        }
    });


});

module.exports = router;

const options = {
    mirror: 'http://libgen.io',
    query: 'cats',
    count: 5,
    sort_by: 'year',
    reverse: true
};

libgen.mirror(function (err, urlString) {
    if (err)
        return console.error(err);
    options.mirror = urlString;
    return console.log(urlString + " is the fasterst mirror");
});


